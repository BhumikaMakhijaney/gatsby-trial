---
path: /job/job2
date: '2018-06-18'
title: Back end developer
---
A back-end developer is a type of programmer who creates the logical back-end and core computational logic of a website, software or information system.
